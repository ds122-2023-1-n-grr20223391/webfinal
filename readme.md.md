# Trabalho Final - Desenvolvimento Web
-----------------------------------------
### A aplicação tem como premissa capturar e exibir ao usuário se as palavras digitadas estão de acordo com o texto de exemplo.

## Descrição:
1. O iníco do jogo se dá com o usuário clicando no botão iniciar e o cronômetro é acionado;
2. Logo após esse evento  o texto modelo é apresentado e o usuário pode começar a digitação;
3. Para cada palavra digitada da forma correta o texto de exemplo fica verde e para cada palavra errada o texto fica vermelho;
4. É possível apagar a palavra digitada parra corrigi-lá;
5. Ao termino da digitação o cronômetro para;
6. Logo em seguida as estatísticas são mostradas, tais como: quantidade de palavras por minuto, precisão da digitação, pontuaçãodo usuário e se o texto está correto ou incorreto.
-----------------------------------------

## Jogo desenvolvido pelos alunos:
* DOUGLAS MUNHOZ STAMATO
* JEFFERSON GUNTHER HOFFMANN
* LUIZ GABRIEL DE OLIVEIRA SANTIN
* RICARDO HENRIQUE GARCIA DURIGON
